# test_cf_mssql.py

import pytest

######################
#
# MSSQL tests
#
######################

MSSQL_HOST = '127.0.0.1'
MSSQL_PORT = 14333
MSSQL_DATABASE = ''
MSSQL_USER = 'sa'
MSSQL_PASSWORD = 'oiwjeflksdf2323!!!'
MSSQL_AUTOCOMMIT = True
MSSQL_DICIONARY_CURSOR = True
MSSQL_ENCODING = 'utf-8'
MSSQL_DRIVER = '{MSSQLSERVER}'

MSSQL_DOCKER_IMAGE = 'microsoft/mssql-server-linux'
MSSQL_CONTAINER_NAME = 'cf_database_test_mssql'
MSSQL_CONTAINER_PORTS = {'1433/tcp': MSSQL_PORT}


@pytest.fixture()
def mssql_server() -> Any:
    """Create a connection to a MSSQL server on a Docker container."""

    container = create_docker_container(
        container_name=MSSQL_CONTAINER_NAME,
        image=MSSQL_DOCKER_IMAGE,
        environment=[
            'MSSQL_ACCEPT_EULA=Y',
            f'MSSQL_SA_PASSWORD={MSSQL_PASSWORD}',
            'MSSQL_PID=Developer'
        ],
        ports=MSSQL_CONTAINER_PORTS
    )
    print(f'Container created: {MSSQL_CONTAINER_NAME}')
    yield container

    # Teardown
    container.remove(force=True)
    print(f'Container removed: {MSSQL_CONTAINER_NAME}')


def test_mssql_container(mssql_server):
    print(f'{str(mssql_server.name)} == {MSSQL_CONTAINER_NAME}')
    assert mssql_server.name == MSSQL_CONTAINER_NAME


def test_mssql_connection(mssql_server):
    database = Database(
        host=MSSQL_HOST,
        port=str(MSSQL_PORT),
        database=MSSQL_DATABASE,
        username=MSSQL_USER,
        password=MSSQL_PASSWORD,
        autocommit=MSSQL_AUTOCOMMIT,
        dictionary_cursor=MSSQL_DICIONARY_CURSOR,
        encoding=MSSQL_ENCODING,
        driver=MSSQL_DRIVER,
    )
    results = database.execute('SELECT @@version')
    print(results[0])
    pass
