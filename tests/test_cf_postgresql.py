# test_cfpostgresql.py

import logging
import re
import pytest
import pandas as pd
from cheesefactory_docker import DockerContainer
from cheesefactory_database.postgresql import CfPostgresql

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(module)-20s line:%(lineno)-4d %(levelname)-8s %(message)s'
)
logger = logging.getLogger(__name__)

#
# Test config
#
POSTGRES_HOST = '127.0.0.1'
POSTGRES_PORT = '5432/tcp'
POSTGRES_CONTAINER_PORT = 54322
POSTGRES_DATABASE = 'postgres'
POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = 'ittybittypassword'
POSTGRES_AUTOCOMMIT = True
POSTGRES_DICIONARY_CURSOR = True
POSTGRES_ENCODING = 'utf8'


#
# Container set up and test.
#

@pytest.fixture(scope='module')
def postgres_container():
    """Create a PostgreSQL server Docker container."""

    return DockerContainer(
        container_name='cf_database_test_postgres',
        image='postgres:9.6.17-alpine',
        ports={POSTGRES_PORT: POSTGRES_CONTAINER_PORT},
        detach=True,
        environment=[
            f'POSTGRES_PASSWORD={POSTGRES_PASSWORD}'
        ],
    )


def test_container(postgres_container):
    """Is the container properly running?"""
    logger.debug(f'Container ID: {str(postgres_container)}')
    version = postgres_container.engine_version
    logger.debug(f'version: {version}')
    assert re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', version) is not None


#
# PostgreSQL connection setup and test.
#

@pytest.fixture(scope='module')
def postgres_server():
    """Establish connection to PostgreSQL server."""
    return CfPostgresql(
        host=POSTGRES_HOST,
        port=str(POSTGRES_CONTAINER_PORT),
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        autocommit=POSTGRES_AUTOCOMMIT,
        dictionary_cursor=POSTGRES_DICIONARY_CURSOR,
        encoding=POSTGRES_ENCODING
    )


def test_postgres(postgres_server):
    """Is the PostgreSQL server running? Tests a non-df SQL execute()"""
    results = postgres_server.execute('SELECT version()')
    logger.debug(f'Find "^PostgreSQL 9.6.17" in results: {str(results[0])}')
    assert re.search('^PostgreSQL 9.6.17', results[0]['version'])


def test_postgres_to_df(postgres_server):
    """Test execute to dataframe."""
    df = postgres_server.execute('SELECT version()', dataframe=True)
    logger.debug(f'dataframe results:\n\n{str(df)}')
    assert df.equals(
        pd.DataFrame(
            [{'version': 'PostgreSQL 9.6.17 on x86_64-pc-linux-musl, compiled by gcc (Alpine 9.2.0) 9.2.0, 64-bit'}]
        )
    )


def test_connection_status(postgres_server):
    status = postgres_server.connection_status()
    logger.debug(f'Connection status: {status}')
    assert status == 'OK', f'Status was not OK: {status}'


def test_split_table_path(postgres_server):
    table_path = 'myschema.mytable'
    logger.debug(f'Splitting table_path: {table_path}')
    schema, table = postgres_server.split_table_path(table_path)
    assert 'myschema' == schema and 'mytable' == table, f'Split not good: schema = {schema}, table={table}'


def test_split_bad_table_path(postgres_server):
    table_path = 'myschemamytable'
    logger.debug(f'Splitting this table_path should fail (missing period): {table_path}')
    try:
        schema, table = postgres_server.split_table_path(table_path)
        logger.debug(f'schema = {schema}, table = {table}')
    except ValueError as e:
        assert f'table_path not in proper <schema>.<table> format: {table_path}', f'Bad happened:\n{e}'


def test_database_exists(postgres_server):
    database_name = 'postgres'
    result = postgres_server.database_exists(database_name)
    assert True is result, f'Database incorrectly found not to exist: {database_name}'


def test_database_does_not_exist(postgres_server):
    database_name = 'does_not_exist'
    result = postgres_server.database_exists(database_name)
    assert False is result, f'Database incorrectly found to exist: {database_name}'


def test_schema_exists(postgres_server):
    schema_name = 'information_schema'
    result = postgres_server.schema_exists(schema_name)
    assert True is result, f'Schema incorrectly found not to exist: {schema_name}'


def test_schema_does_not_exist(postgres_server):
    schema_name = 'does_not_exist'
    result = postgres_server.schema_exists(schema_name)
    assert False is result, f'Schema incorrectly found to exist: {schema_name}'


def test_table_exists(postgres_server):
    table = 'information_schema.sql_languages'
    logger.debug(f'Testing for the existence of table: {table}')
    exist = postgres_server.table_exists(table)
    assert exist is True, f'exist should be True, Instead, it is {exist}'


def test_table_does_not_exist(postgres_server):
    table = 'information_schema.sql_languages_doesnt_exist'
    logger.debug(f'Testing for the existence of table: {table}')
    exist = postgres_server.table_exists('information_schema.sql_languages_doesnt_exist')
    assert exist is False, f'exist should be False, Instead, it is {exist}'


def test_fields_exist(postgres_server):
    exist = postgres_server.fields_exist(
        'information_schema.sql_languages',
        ['sql_language_source', 'sql_language_year', 'sql_language_integrity']
    )
    assert exist is True, f'exist should be True. Instead, it is {exist}'


def test_fields_does_not_exist(postgres_server):
    exist = postgres_server.fields_exist(
        'information_schema.sql_languages',
        ['sql_language_source', 'sql_language_year_doesnt_exist', 'sql_language_integrity']
    )
    assert exist is False, f'exist should be False. Instead, it is {exist}'


def test_no_fetchall(postgres_server):
    results = postgres_server.execute('SELECT version()', fetchall=False)
    assert None is results, f'Should have received None. Instead got: {results}'


def test_get_primary_key(postgres_server):
    # Sometimes the container doesn't get reset, so perform inserts with conflicts in mind.
    postgres_server.execute('''
        CREATE TABLE IF NOT EXISTS music (
          id    integer PRIMARY KEY,
          name  varchar(40));
        INSERT INTO music (id, name) VALUES (1, 'John Williams') ON CONFLICT (id) DO NOTHING;
        INSERT INTO music (id, name) VALUES (2, 'Hans Zimmer') ON CONFLICT (id) DO NOTHING;
        INSERT INTO music (id, name) VALUES (3, 'James Horner') ON CONFLICT (id) DO NOTHING;
        INSERT INTO music (id, name) VALUES (4, 'Clint Mansell') ON CONFLICT (id) DO NOTHING; 
    ''', fetchall=False)

    results = postgres_server.get_primary_keys('music')
    assert results == ['id'], f'Did not correctly find the primary key. Found: {str(results)}'


def test_get_primary_keys(postgres_server):
    # Sometimes the container doesn't get reset, so perform inserts with conflicts in mind.
    postgres_server.execute('''
        CREATE TABLE IF NOT EXISTS movies (
          id1   integer,
          id2   integer,
          name  varchar(40),
          PRIMARY KEY (id1, id2));
        INSERT INTO movies (id1, id2, name) VALUES (1, 1, 'Star Wars') ON CONFLICT (id1, id2) DO NOTHING;
        INSERT INTO movies (id1, id2, name) VALUES (2, 2, 'Empire Strikes Back') ON CONFLICT (id1, id2) DO NOTHING;
        INSERT INTO movies (id1, id2, name) VALUES (3, 1, 'Return of the Jedi') ON CONFLICT (id1, id2) DO NOTHING;
        INSERT INTO movies (id1, id2, name) VALUES (4, 3, 'Phantom Menace') ON CONFLICT (id1, id2) DO NOTHING; 
    ''', fetchall=False)

    results = postgres_server.get_primary_keys('movies')
    assert results.sort() == ['id1', 'id2'].sort(), f'Did not correctly find the primary key. Found: {str(results)}'


def test_quote_reserved_words():
    word_list = ['hello', 'there', 'buddy', 'DEFAULT', 'where', 'asc']
    logger.debug(f'my word list: {word_list}')
    word_list = CfPostgresql.quote_reserved_words(word_list)
    logger.debug(f'updated word list: {word_list}')
    assert word_list == ['hello', 'there', 'buddy', '"DEFAULT"', '"where"', '"asc"']
